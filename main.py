from nltk.corpus import product_reviews_1, product_reviews_2
from nltk.parse import FeatureEarleyChartParser
from nltk import grammar, word_tokenize
import time

from nltk.draw import draw_trees

from baseline.ssap import sentiment

import string

GOOD = ['', 'output/Good1.txt', 'output/Good2.txt']
FALSE = ['', 'output/False1.txt', 'output/False2.txt']

def sign(n):
    return 1 if n > 0 else -1 if n < 0 else 0

def preprocess(tokens):
    processedTokens = []
    skip = 0
    for i, token in enumerate(tokens):
        if skip > 0:
            skip -= 1
            continue
        if token[0] in ['.', ',', '!', '?'] and i == 0:
            #ignore up any residual punctuation at the beginning of the sentence
            continue
        elif len(token) == 2 and token[0] == ')' and token[1] == ',':
            #fix punctuation after parentheses
            processedTokens += [')', ',']
        elif token == 'don' or token == 'doesn':
            #fix mistokenized "don't"s
            processedTokens += [token[:-1], 'n']
        elif token == "n't":
            #fix mistokenized "n't"s
            processedTokens += ["n", "'", "t"]
        elif token.isdigit():
            #separate digits of numbers
            processedTokens += list(token)
        elif token[-1] == 's' and token[:-1].isdigit():
            #separate decades (e.g. 90s into 9, 0, and s)
            processedTokens += list(token)
        elif len(token)>1 and token[0] == "'" and token[1] !="'" :
            #separate leading apostrophes (unless they are )
            processedTokens += token[0]
            processedTokens.append(token[1:])
        elif token[-2:] in ['st', 'nd', 'rd', 'th', 'am', 'pm'] and token[:-2].isdigit():
            #separate numeric ordinals
            processedTokens += list(token[:-2])
            processedTokens.append(token[-2:])
        elif ':' in token and token[:token.find(':')].isdigit() and token[token.find(':')+1:].isdigit():
            #separate times expressed as hh:mm
            processedTokens += list(token[:token.find(':')])
            processedTokens.append(':')
            processedTokens += list(token[token.find(':')+1:])
        else:
            processedTokens.append(token)
    return processedTokens

def readTheLabel(label):
    value = {'pos': 1, 'neg': -1}
    if 'SENT' not in label or str(label['SENT']) == '?v':
        return 0
    if('NEG' in label and label['NEG']):
        return -value[label['SENT']]
    else:
        return value[label['SENT']]


def sentimentParse(parser, sentence):

    processedSentence = preprocess(sentence)
    print(processedSentence)
    try:
        trees = list(parser.parse(processedSentence))
        if len(trees) == 0:
            return None
        else:
            #trees[0].draw()
            v = 0
            for tree in trees:
                #print(readTheLabel(tree.label()))
                #tree.draw()
                v += readTheLabel(tree.label())
            return v
    except ValueError as ve:
        print(ve)
        return None

def printToFile(phase, sentence, guess, ground):
    #phase is either 1 (training) or 2 (testing)
    sentenceForPrint = ' '.join(sentence)
    if guess == None:
        #did not parse
        with open(FALSE[phase],'a') as false:
            false.write(f'Sentence "{sentenceForPrint}"' + ' did not parse\n')
        return

    guessForPrint = 'positive' if guess > 0 else 'negative' if guess < 0 else 'neutral'
    groundForPrint = 'positive' if ground > 0 else 'negative' if ground < 0 else 'neutral'

    if guess == 0:
        #predicted neutral
        with open(FALSE[phase],'a') as false:
            false.write(f'Sentence "{sentenceForPrint}"' + f' predicted {guessForPrint} instead of {groundForPrint}\n')
    elif guess * ground < 0:
        #wrong prediction
        with open(FALSE[phase],'a') as false:
            false.write(f'Sentence "{sentenceForPrint}"' + f' predicted {guessForPrint} instead of {groundForPrint}\n')
    else:
        #correct prediction
        with open(GOOD[phase],'a') as good:
            good.write(f'Sentence "{sentenceForPrint}"' + f' predicted correctly as {guessForPrint}\n')
###########


open(GOOD[1], "w+")
open(GOOD[2], "w+")
open(FALSE[1], "w+")
open(FALSE[2], "w+")

with open('Delivery/sentimentgrammar.fcfg') as gf:
    g = grammar.FeatureGrammar.fromstring(gf.read())

p = FeatureEarleyChartParser(g)

## TRAINING ##

total = 49
target = 49

##process REVIEWS 1
#
#for fileid in product_reviews_1.fileids()[:]:
#    #for each product
#    for review in product_reviews_1.reviews(fileid)[:5]:
#
#        #for the first 3 reviews
#        for line in review.review_lines[:3]:
#
#            features = line.features
#            scores = list(map(lambda tup: int(tup[1]), features))
#            score = sum(scores)
#            if score != 0:
#
#                if target < 0:
#                    target -=1
#                    continue
#                else:
#                    target -= 1
#                    
#                #for each sentiment-loaded sentence
#                print(f'\n({index})')
#                index += 1
#                print(' '.join(line.sent))
#                print(f'Sentence is {"pos" if score > 0 else "neg"}') #print the real sentiment
#                afinn = sentiment(line.sent)
#                print(f'AFINN guessed {"pos" if afinn > 0 else "neg"} ({afinn})' if afinn != 0 else 'AFINN failed') #print the AFINN guess
#                guess = sentimentParse(p, line.sent[:])
#                if guess != None :
#                    parsedOurs += 1
#                    if guess * score > 0 : correctOurs += 1
#                print(f'Ours guessed {"pos" if guess > 0 else "neg"} ({guess})' if guess != None and guess != 0 else f'Ours failed ({"0" if guess != None else "did not parse"})') #print our guess
#                printToFile(1, line.sent, guess, score)
#             
##PROCESS REVIEWS 2
#for fileid in product_reviews_2.fileids()[:]:
#    #for each product
#    for review in product_reviews_2.reviews(fileid)[:5]:
#        #for the first 3 reviews
#        for line in review.review_lines[:3]:
#            features = line.features
#            scores = list(map(lambda tup: int(tup[1]), features))
#            score = sum(scores)
#            if score != 0:
#
#                if target < 0:
#                    target -=1
#                    continue
#                else:
#                    target -= 1
#
#                #for each sentiment-loaded sentence
#                print(f'\n({index})')
#                index += 1
#                print(' '.join(line.sent))
#                print(f'Sentence is {"pos" if score > 0 else "neg"}') #print the real sentiment
#                afinn = sentiment(line.sent)
#                print(f'AFINN guessed {"pos" if afinn > 0 else "neg"} ({afinn})' if afinn != 0 else 'AFINN failed') #print the AFINN guess
#                guess = sentimentParse(p, line.sent[:])
#                if guess != None : 
#                    parsedOurs += 1
#                    if guess * score > 0 : correctOurs += 1
#                print(f'Ours guessed {"pos" if guess > 0 else "neg"} ({guess})' if guess != None and guess != 0 else f'Ours failed ({"0" if guess != None else "did not parse"})') #print our guess
#                printToFile(1, line.sent, guess, score)

## TESTING OURS ##

with open('questionable-test.txt','r') as testdata:
    rawdata = testdata.readlines()
    testdata = list(map(lambda l: {'label': l[:3], 'sent': word_tokenize(l[4:])}, rawdata))

def fmeas(precision, recall):
    return 2 * (precision*recall) / (precision+recall)

ourTruePositive = 0
ourTrueNegative = 0
ourFalsePositive = 0
ourFalseNetural = 0
ourFalseNegative = 0
didNotParse = 0

index = 0
gsPositive = 0
gsNegative = 0

startTime = time.time()

for i, line in enumerate(testdata):
    
    score = line['label']
    if score == 'pos':
        gsPositive += 1
    else:
        gsNegative += 1
    guess = sentimentParse(p, line['sent'][:])
    print(f'Sentence is {score}')
    print(f'Ours guessed {"pos" if guess > 0 else "neg"} ({guess})' if guess != None and guess != 0 else f'Ours failed ({"0" if guess != None else "did not parse"})')
    print('\n')
    #printToFile(2, line['sent'], guess, 1 if score == 'pos' else -1)
    score = 1 if score=='pos' else -1
    if guess == None:
        didNotParse += 1
        continue
    if guess * score > 0:
        #afinn agrees with GS
        if guess > 0:
            #positively
            ourTruePositive += 1
        else:
            #negatively
            ourTrueNegative += 1
    elif guess * score < 0:
    #afinn disagrees with GS
        if guess > 0:
            #positively
            ourFalsePositive += 1
        else:
            #negatively
            ourFalseNegative += 1
    else:
        #afinn wrongly predicted neutral
        ourFalseNetural += 1


endTime = time.time()

#print('\nOur system')
#print('\n## GENERAL DATA ##')
#print(f'Analyzed {index} sentences in {endTime-startTime} seconds.')
#print(f'Golden standard positives: {gsPositive}')
#print(f'Golden standard negatives: {gsNegative}')
#print('\n## RAW METRICS ##')
#print(f'True positives: {ourTruePositive}')
#print(f'True negatives: {ourTrueNegative}')
#print(f'False positives: {ourFalsePositive}')
#print(f'False neutrals: {ourFalseNetural}')
#print(f'False negatives: {ourFalseNegative}')
#print(f'Sentences that didn\'t parse: {didNotParse}')
#print('\n## ACCURACY METRICS ##')
#ourPrecisionPositive = ourTruePositive/(ourTruePositive+ourFalsePositive)
#print(f'Positive precision: {ourPrecisionPositive}')
#ourRecallPositive = ourTruePositive/gsPositive
#print(f'Positive recall: {ourRecallPositive}')
#print(f'Positive F-measure: {fmeas(ourPrecisionPositive, ourRecallPositive)}')
#ourPrecisionNegative = ourTrueNegative/(ourTrueNegative+ourFalseNegative)
#print(f'Negative precision: {ourPrecisionNegative}')
#ourRecallNegative = ourTrueNegative/gsNegative
#print(f'Negative recall: {ourRecallNegative}')
#print(f'Negative F-measure: {fmeas(ourPrecisionNegative, ourRecallNegative)}')
#print('\n## AGGREGATE ACCURACY METRICS ##')
#ourMicroPresion = (ourTruePositive + ourTrueNegative) / (ourTruePositive + ourFalsePositive + ourTrueNegative + ourFalseNegative)
#print(f'Overall micro-precision: {ourMicroPresion}')
#ourMicroRecall = (ourTruePositive + ourTrueNegative) / (gsPositive + gsNegative)
#print(f'Overall micro-recall: {ourMicroRecall}')
#print(f'Overall micro-F-measure: {fmeas(ourMicroPresion, ourMicroRecall)}')
#print('\n')





## TESTING SSAP ##

afinnTruePositive = 0
afinnTrueNegative = 0
afinnFalsePositive = 0
afinnFalseNetural = 0
afinnFalseNegative = 0

index = 0
gsPositive = 0
gsNegative = 0

#startTime = time.time()
#
### TEST SSAP on test data
#
#for i, line in enumerate(testdata):
#    
#    score = line['label']
#    if score == 'pos':
#        gsPositive += 1
#    else:
#        gsNegative += 1
#    guess = sentiment(line['sent'][:])
#    #print(' '.join(line['sent']))
#    #print(f'Sentence is {score}')
#    #print(f'SSAP guessed {"pos" if guess > 0 else "neg"} ({guess})' if guess != None and guess != 0 else f'SSAP failed ({"0" if guess != None else "did not parse"})')
#    #print('\n')
#    #printToFile(2, line['sent'], guess, 1 if score == 'pos' else -1)
#    score = 1 if score=='pos' else -1
#    if guess * score > 0:
#        #afinn agrees with GS
#        if guess > 0:
#            #positively
#            afinnTruePositive += 1
#        else:
#            #negatively
#            afinnTrueNegative += 1
#    elif guess * score < 0:
#    #afinn disagrees with GS
#        if guess > 0:
#            #positively
#            afinnFalsePositive += 1
#        else:
#            #negatively
#            afinnFalseNegative += 1
#    else:
#        #afinn wrongly predicted neutral
#        afinnFalseNetural += 1
#
#endTime = time.time()



##test SSAP on REVIEWS 1
#
#for fileid in product_reviews_1.fileids()[:]:
#    
#    for review in product_reviews_1.reviews(fileid)[:]:
#
#        for line in review.review_lines[:]:
#
#            features = line.features
#            scores = list(map(lambda tup: int(tup[1]), features))
#            score = sum(scores)
#            if score != 0:
#
#                if score > 0:
#                    gsPositive +=1
#                else:
#                    gsNegative +=1
#                    
#                #for each sentiment-loaded sentence
#                #print(f'\n({index})')
#                index += 1
#                #print(' '.join(line.sent))
#                #print(f'Sentence is {"pos" if score > 0 else "neg"}') #print the real sentiment
#                afinn = sentiment(line.sent)
#                #print(f'AFINN guessed {"pos" if afinn > 0 else "neg"} ({afinn})' if afinn != 0 else 'AFINN failed') #print the AFINN guess
#                if afinn * score > 0:
#                    #afinn agrees with GS
#                    if afinn > 0:
#                        #positively
#                        afinnTruePositive += 1
#                    else:
#                        #negatively
#                        afinnTrueNegative += 1
#                elif afinn * score < 0:
#                #afinn disagrees with GS
#                    if afinn > 0:
#                        #positively
#                        afinnFalsePositive += 1
#                    else:
#                        #negatively
#                        afinnFalseNegative += 1
#                else:
#                    #afinn wrongly predicted neutral
#                    afinnFalseNetural += 1
#             
##test SSAP on REVIEWS 2
#for fileid in product_reviews_2.fileids()[:]:
#    #for each product
#    for review in product_reviews_2.reviews(fileid)[:]:
#        #for the first 3 reviews
#        for line in review.review_lines[:]:
#            features = line.features
#            scores = list(map(lambda tup: int(tup[1]), features))
#            score = sum(scores)
#            if score != 0:
#
#                if score > 0:
#                    gsPositive +=1
#                else:
#                    gsNegative +=1
#
#                #for each sentiment-loaded sentence
#                #print(f'\n({index})')
#                index += 1
#                #print(' '.join(line.sent))
#                #print(f'Sentence is {"pos" if score > 0 else "neg"}') #print the real sentiment
#                afinn = sentiment(line.sent)
#                #print(f'AFINN guessed {"pos" if afinn > 0 else "neg"} ({afinn})' if afinn != 0 else 'AFINN failed') #print the AFINN guess
#                if afinn * score > 0:
#                    #afinn agrees with GS
#                    if afinn > 0:
#                        #positively
#                        afinnTruePositive += 1
#                    else:
#                        #negatively
#                        afinnTrueNegative += 1
#                elif afinn * score < 0:
#                #afinn disagrees with GS
#                    if afinn > 0:
#                        #positively
#                        afinnFalsePositive += 1
#                    else:
#                        #negatively
#                        afinnFalseNegative += 1
#                else:
#                    #afinn wrongly predicted neutral
#                    afinnFalseNetural += 1
#
#endTime = time.time()


## PRINT RESULTS ##

#print('\nSSAP with AFINN')
#print('\n## GENERAL DATA ##')
#print(f'Analyzed {index} sentences in {endTime-startTime} seconds.')
#print(f'Golden standard positives: {gsPositive}')
#print(f'Golden standard negatives: {gsNegative}')
#print('\n## RAW METRICS ##')
#print(f'True positives: {afinnTruePositive}')
#print(f'True negatives: {afinnTrueNegative}')
#print(f'False positives: {afinnFalsePositive}')
#print(f'False neutrals: {afinnFalseNetural}')
#print(f'False negatives: {afinnFalseNegative}')
#print('\n## ACCURACY METRICS ##')
#precisionPositive = afinnTruePositive/(afinnTruePositive+afinnFalsePositive)
#print(f'Positive precision: {precisionPositive}')
#recallPositive = afinnTruePositive/gsPositive
#print(f'Positive recall: {recallPositive}')
#print(f'Positive F-measure: {fmeas(precisionPositive, recallPositive)}')
#precisionNegative = afinnTrueNegative/(afinnTrueNegative+afinnFalseNegative)
#print(f'Negative precision: {precisionNegative}')
#recallNegative = afinnTrueNegative/gsNegative
#print(f'Negative recall: {recallNegative}')
#print(f'Negative F-measure: {fmeas(precisionNegative, recallNegative)}')
#print('\n## AGGREGATE ACCURACY METRICS ##')
#microPrecision = (afinnTruePositive + afinnTrueNegative) / (afinnTruePositive + afinnFalsePositive + afinnTrueNegative + afinnFalseNegative)
#print(f'Overall micro-precision: {microPrecision}')
#microRecall = (afinnTruePositive + afinnTrueNegative) / (gsPositive + gsNegative)
#print(f'Overall micro-recall: {microRecall}')
#print(f'Overall micro-F-measure: {fmeas(microPrecision, microRecall)}')
#print('\n')