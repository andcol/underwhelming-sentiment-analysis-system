from nltk.corpus import product_reviews_1, product_reviews_2

for fileid in product_reviews_2.fileids():
    print(fileid)
    with open(f'corpus/{fileid}', 'w+') as of:
        of.write(product_reviews_2.raw(fileid))