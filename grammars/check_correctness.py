with open("./grammars/sentimentgrammar.fcfg", "r") as grammar:
    for i, line in enumerate(grammar):
        trimmed = line.rstrip()
        if(len(trimmed) > 0 and trimmed[-1] == '|'):
            print("Dangerous empty production in line " + str(i+1))